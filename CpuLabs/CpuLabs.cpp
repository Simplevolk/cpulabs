// CpuLabs.cpp : main project file.

#include "stdafx.h"
#include <stdlib.h>

using namespace System;

//��������� ����� ��������� ������
struct Task //��� ������ ����� ������
{
	int Priority;//���� ������ ���������
	int TimeExecutionInterval;//�����,�� ������� ����� ��������� ������
	Task *Next; //��������� �� ��������� �����
};

/*���� ���������*/
struct Stack
{
	Task task;  //�������������� �������
	Stack *Next, *Head; //������ ����� � ��������� �� ��������� �������
};

/*������� ���������� �������� � ���� (� ������ LIFO)*/
void Add(Task task, Stack *&MyList) //��������� ������� ����� � ��������� �� ����, ��� ���� �������, ��� ����������� ��������� ����� ��� �� ���� ����������
{
	Stack *temp = new Stack; //�������� ������ ��� ������ ��������
	temp->task = task; //���������� � ���� task ����������� � ������� ������� task
	temp->Next = MyList->Head; //���������, ��� ��������� ������� ��� ����������
	MyList->Head = temp; //�������� ������ �� ������� ������
}

/*������� ����������� �����*/
void Show(Stack *MyList) //����� ������ ��� ����
{
	Stack *temp = MyList->Head; //��������� ��������� � ��������� ���, ��� ��� ������� � ������ �����
							   //� ������� ����� �������� �� ����� �����
	while (temp != NULL) //������� ��� ������� � ������ �����
	{
	//	cout << temp->task << " "; //������� �� ����� ������� �����
		temp = temp->Next; //��������� � ���������� ��������
	}
}

/*������� �������� ����� �� ������*/
void ClearList(Stack *MyList)
{
	while (MyList->Head != NULL) //���� �� ������ �� �����
	{
		Stack *temp = MyList->Head->Next; //��������� ���������� ��� �������� ������ ���������� ��������
		delete MyList->Head; //����������� ����� ������������ ������
		MyList->Head = temp; //������ ����� �� ���������
	}
}




//����� ������
struct Queue
{
	Task *Head, *Tail; //��������� �� ������ ������ � �� �����
public:
	Queue() :Head(NULL), Tail(NULL) {}; //������������� ���������� ��� ������
	~Queue(); //���������� ��� ������������ ������ �� ����� ��� �����
	void Add(int priority,int timeInterval); //������� ���������� ��������� Task � ���������� � � ������ ��� ��������
	void Show(); //������� ����������� ������ Queue 
	int Count;
};


struct CPU
{
	Stack *stack =new Stack;
	Task *_previousTask;
	int TimeCount = 0;//���������� �����
	void Execute(Task *task);


};

void CPU::Execute(Task *task)
{
	if (_previousTask == NULL)
		_previousTask = task;


	if(TimeCount==task->TimeExecutionInterval)//������ ����� ����������� ���� ������
	{
		TimeCount = 0;
		_previousTask = task;

		//��������� ����� ������ �� ���������
		if(task->Priority>_previousTask->Priority)//����� ������ ������������
		{
			Add(*_previousTask, stack);//�������� ������ ������ � ����
		}
		else
		{
			Add(*task, stack);//�������� ����� ������ � �������-����� ��������
		}

		return;
	}


	TimeCount++;

	

	

	return;
}

Queue::~Queue() //���������� ��� ������� ������
{
	Task *temp = Head; //��������� ��������� �� ������ ������
	while (temp != NULL) //���� � ������ ���-�� ����

	{
		temp = Head->Next; //������ ������ �� ��������� ������� ������
		delete Head;  //������������ ������ �� ������ ��������� ��� �������� ������

		Head = temp;  //����� ������ �� ��������� �����, ������� ����� �� �������

	}
}

//������� ���������� �������������� ����� ��������� NODE � ���������� ���� ��������� � ������
void Queue::Add(int priority,int timeInterval)
{
	Task *temp = new Task; //��������� ������ ��� ������ ����� ������
	temp->Priority = priority; //��������� ����������� ��������� ��������� priority
	temp->Next = NULL; //��������, ��� ��������� ����� ������������� ��������� ���� ������
	temp->TimeExecutionInterval = timeInterval;
	if (Head != NULL) //���� ������ �� ���� 
	{
		Tail->Next = temp; //��������, ��� ��������� ����� ������ ��� ������������� ���������
		Tail = temp;
	}
	else Head = Tail = temp; //���� ������ �� ����, ���������� ������� ��������

	Count++;
}

//������� ����������� ������ �� ������
void Queue::Show()
{
	Task *temp = Head; //��������� ��������� �� ������ ������
	while (temp != NULL) //���� � ������ ���-�� �����������
	{
	//	cout << temp->priority << " "; //������� �������� �� ������ �� �����
		temp = temp->Next; //�������� ��������� �� ������ �� ����� ���������� ��������
	}
//	cout << endl;
}





int main(array<System::String ^> ^args)
{
    Console::WriteLine(L"Hello World");

	CPU *cpu = new CPU;

	Task *task = new Task;

	Queue *queue0 = new Queue;
	Queue *queue1 = new Queue;
	Queue *queue2 = new Queue;
	
	
	for (int i = 0; i < 100; i++)
	{
		if (i % 2 == 0)
		{
			queue0->Add(0, i % 3);
		}
		else if (i % 3 == 1)
		{
			queue1->Add(1, i % 2);
		}
		else
		{
			queue2->Add(2, i % 5);
		}




		int countQ0 = queue0->Count;
		while (countQ0 != 0)
		{
			cpu->Execute(task);
			countQ0--;
		}

		int countQ1 = queue1->Count;
		while (countQ1 != 0)
		{
			cpu->Execute(task);
			countQ1--;
		}

		int countQ2 = queue2->Count;
		while (countQ2 != 0)
		{
			cpu->Execute(task);
			countQ2--;
		}

	}
    return 0;
}
